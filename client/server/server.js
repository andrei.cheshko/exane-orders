const express = require('express');
const path = require('path');
const app = express();

app.use('/static', express.static(path.join(__dirname, '../build')));

app.get('/*', (req,res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'))
});

app.listen(3000, function() {
  console.log('Listening on localhost:3000');
})
