import React from 'react';
import Orders from './pages/Orders.jsx';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { fetchOrders } from './actions/actions.js';
import { ordersReducer } from './reducers/reducers.js';
import { Provider, connect } from 'react-redux';

const loggerMiddleware = createLogger();

const POLLING_FREQ_IN_SECS = 10;

const store = createStore(
  ordersReducer,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
);

const mapStateToProps = (state) => {
  return {
    orders: state.orders
  }
}

const OrdersPage = connect(
  mapStateToProps
)(Orders);

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    store.dispatch(fetchOrders(false));

    if (POLLING_FREQ_IN_SECS > 0) {
      setInterval(() => {
        store.dispatch(fetchOrders(true));
      }, 1000*POLLING_FREQ_IN_SECS);
    }
  }

  render() {
    return (
      <Provider store={store}>
        <OrdersPage />
      </Provider>
    );
  }
}
