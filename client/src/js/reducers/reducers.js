import {
  ORDERS_FETCH_BEGIN,
  ORDERS_FETCH_SUCCESS,
  ORDERS_FETCH_ERROR
} from '../actions/actionTypes.js';

const initialState = {
  orders: [],
  isFetching: false
}

export function ordersReducer(state = initialState, action) {
  switch(action.type) {
    case ORDERS_FETCH_BEGIN: {
      return Object.assign({}, state, {isFetching: true});
    }
    case ORDERS_FETCH_SUCCESS: {
      return Object.assign({}, {isFetching: false}, {orders: action.orders.concat(state.orders)});
    }
    case ORDERS_FETCH_ERROR: {
      return Object.assign({}, state, {isFetching: false});
    }
    default:
      return state;
  }
};
