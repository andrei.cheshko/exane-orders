import React from 'react';
import Select from 'react-select';
import AGGREGATE from '../constants/aggregations.js';
import AggregatedOrders from '../components/AggregatedOrders.jsx';
import OrderList from '../components/OrderList.jsx';

export default class Orders extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      aggregateBy: AGGREGATE.SECTOR,
      filter: null
    };

    this._aggregateChanged = this._aggregateChanged.bind(this);
    this._showAggregatedOrders = this._showAggregatedOrders.bind(this);
  }

  _aggregateChanged(selection) {
    this.setState({
      aggregateBy: selection,
      filter: null
    });
  }

  _showAggregatedOrders(groupName, groupKey) {
    this.setState({
      aggregateBy: AGGREGATE.NONE,
      filter: {groupName, groupKey}
    });
  }

  render() {

    const groups = Object.keys(AGGREGATE).map((group, index) => {
      return {label: AGGREGATE[group].label, value: AGGREGATE[group].value};
    });

    let partial = <div className='loading'>Loading...</div>;

    // Mount Tables only when we loaded 1st batch of orders
    if (this.props.orders.length > 0) {

      if (this.state.aggregateBy.value !== 'none') {
        partial = <AggregatedOrders orders={this.props.orders}
                                    aggregateBy={this.state.aggregateBy}
                                    onAggregatedClicked={this._showAggregatedOrders} />
      } else {
        partial = <OrderList orders={this.props.orders} filter={this.state.filter} />
      }
    }

    return (
      <div>
        <h1>Orders</h1>
        <section className='aggregate-section'>
          <label>Aggregated by:</label>
          <Select name='aggregated-by'
                  value={this.state.aggregateBy}
                  options={groups}
                  onChange={this._aggregateChanged}
                  className='selector'
                  clearable={false} />
        </section>

        {partial}
      </div>
    );
  }
}
