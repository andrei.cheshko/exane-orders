import React from 'react';
import FilterCell from './FilterCell.jsx';
import Row from './Row.jsx';

class Table extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      filters: this.props.filters,
      filteredRows: this.filterRows(this.props.rows, this.props.filters),
      isUpdate: false
    };

    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.onClearFilters = this.onClearFilters.bind(this);
    this.filterRows = this.filterRows.bind(this);
    this.onRowClicked = this.onRowClicked.bind(this);
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.rows != nextProps.rows) {
      this.setState({
        filteredRows: this.filterRows(nextProps.rows, this.state.filters),
        isUpdate: true
      })
    }
  }

  filterRows(rows, filters) {
    const filterKeys = Object.keys(filters);

    // Skip filtering if no filter is specified
    if (filterKeys.length === 0) {
      return rows;
    }

    let filteredRows = rows.filter((row) => {
      let keep = true;
      filterKeys.forEach((filterId, index) => {
        if (!row[filterId].toString().toLowerCase().includes(filters[filterId].toLowerCase())) {
          keep = false;
        }
      });
      return keep;
    });

    return filteredRows;
  }

  handleFilterChange(value, id) {
    let filters = Object.assign({}, this.state.filters);
    if (value !== '') {
      filters[id] = value;
    }
    else {
      delete filters[id];
    }

    this.setState({ filters, filteredRows: this.filterRows(this.props.rows, filters) });
  };

  onClearFilters() {
    // all filters removed
    this.setState({filters: {} });
  };

  onRowClicked(rowId, cols) {
    if (typeof this.props.onRowClicked !== 'undefined') {
      this.props.onRowClicked(rowId, cols);
    }
  }

  render() {
    const rows = this.state.filteredRows.map((cols, rowIndex) => {
      return <Row rowId={rowIndex}
                  cols={cols}
                  onClickCallback={this.onRowClicked}
                  key={`r-${rowIndex}`} />
    });

    const headers = this.props.columns.map((column, index) => {
      const style = column.width ? {minWidth: `${column.width}px`} : {};
      return <th key={`th-${index}`} style={style}>{column.name}</th>;
    });

    let filters = null;
    if (this.props.showFilters) {
      const filterRows = this.props.columns.map((column, index) => {
        const filterValue = this.state.filters[index] ? this.state.filters[index] : '';
        return <th key={`filter-${index}`}><FilterCell id={index} onChange={this.handleFilterChange} value={filterValue} /></th>
      });
      filters = <tr className='filters'>{filterRows}</tr>;
    }

    return (
      <table className='order-table'>
        <thead>
          <tr>{headers}</tr>
          {filters}
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  };
}

Table.propTypes = {
  filters: React.PropTypes.object,
  columns: React.PropTypes.array.isRequired,
  rows: React.PropTypes.array.isRequired
};

Table.defaultProps = {
  filters: {}
};

export default Table;
