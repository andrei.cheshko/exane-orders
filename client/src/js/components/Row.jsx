import React from 'react';

class Row extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      isUpdate: this.props.isUpdate
    };

    this._row = null;

    this._onClicked = this._onClicked.bind(this);
  }

  _onClicked() {
    if (typeof this.props.onClickCallback !== 'undefined') {
      this.props.onClickCallback(this.props.rowId, this.props.cols);
    }
  }

  componentDidMount() {
    if (this.state.isUpdate) {
      setTimeout(()=> {
        this.setState({isUpate: false});
      }, 500)
    }
  }

  render() {
    // deconstruct props we are consuming
    const {cols,rowId,onClickCallback} = this.props;

    const columns = cols.map((col, colIndex) => {
      return <td key={`c-${colIndex}`}>{col}</td>;
    });

    return (
      <tr className={this.state.isUpdate ? 'flash' : ''}
          onClick={this._onClicked}
          ref={c => this._row = c}>
        {columns}
      </tr>
    )
  }
}

Row.propTypes = {
  cols: React.PropTypes.array.isRequired,
  rowId: React.PropTypes.number.isRequired,
  onClickCallback: React.PropTypes.func,
  isUpdate: React.PropTypes.bool
};

export default Row;
