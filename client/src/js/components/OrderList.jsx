import React from 'react';
import Select from 'react-select';
import AGGREGATE from '../constants/aggregations.js';
import Table from './Table.jsx';

class OrderList extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    const columns = [
      {name: 'Symbol'},
      {name: 'Side'},
      {name: 'Quantity'},
      {name: 'Sector'},
      {name: 'Client'},
      {name: 'Client Type'},
      {name: 'Trader'}
    ];

    let filters = {};
    if (this.props.filter) {
      for (let i = 0; i < columns.length; i++) {
        if (columns[i].name === this.props.filter.groupName) {
          filters[i] = this.props.filter.groupKey;
          break;
        }
      }
    }

    const rows = this.props.orders.map((order, index) => {
      return [order.symbol, order.side, order.quantity, order.sector, order.client, order.clientType, order.trader];
    });

    return (
      <div>
        <Table columns={columns} rows={rows} showFilters={true} filters={filters} />
      </div>
    );
  }
}

OrderList.propTypes = {
  filter: React.PropTypes.object,
  orders: React.PropTypes.array.isRequired
};

export default OrderList;
