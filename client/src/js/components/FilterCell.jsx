import React from 'react';

class FilterCell extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: this.props.value}

    this._onChange = this._onChange.bind(this);
  }

  _onChange(event) {
    this.setState({value: event.target.value});
    this.props.onChange(event.target.value, this.props.id);
  }

  render() {
    return (
      <input type='text' value={this.state.value} onChange={this._onChange} />
    )
  }
}

FilterCell.propTypes = {
  value: React.PropTypes.string
};

FilterCell.defaultProps = {
  value: ''
};


export default FilterCell;
