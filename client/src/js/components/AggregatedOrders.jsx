import React from 'react';
import AGGREGATE from '../constants/aggregations.js';
import Table from './Table.jsx';
import {PieChart, Pie, Legend, Tooltip, Cell} from 'recharts';

class AggregatedOrders extends React.Component{
  constructor(props) {
    super(props);

    this._aggregateOrders = this._aggregateOrders.bind(this);
    this._onTableRowClicked = this._onTableRowClicked.bind(this);
    this._onPieClicked = this._onPieClicked.bind(this);
  }

  _aggregateOrders(orders, value) {
    let map = new Map();
    orders.map((order, index) => {
      const aggValue = order[value];
      const qty = map.get(aggValue);
      if (typeof qty !== 'undefined') {
        map.set(aggValue, qty + order.quantity);
      }
      else {
        map.set(aggValue, order.quantity);
      }
    });
    return map;
  }

  _onTableRowClicked(rowId, cols) {
    this.props.onAggregatedClicked(this.props.aggregateBy.label, cols[0]);
  }

  _onPieClicked(segment) {
    this.props.onAggregatedClicked(this.props.aggregateBy.label, segment.name);
  }

  render() {
    let aggregateMap = this._aggregateOrders(this.props.orders, this.props.aggregateBy.value);

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#FFC8A2'];

    let rows = [];

    aggregateMap.forEach((value, key) => {
      rows.push([key, value]);
    });

    const pieData = rows.map((row, index) => {
      return {name: row[0], value: row[1]};
    });

    const columns = [{name: this.props.aggregateBy.label, width: 300}, {name: 'Quantity', width: 100}];

    return (
      <div className='data-container'>
        <section>
          <Table columns={columns} rows={rows} showFilters={false} onRowClicked={this._onTableRowClicked}/>
        </section>

        <section>
          <PieChart width={400} height={250}>
            <Pie isAnimationActive={false} data={pieData} cx={200} cy={125} outerRadius={80} onClick={this._onPieClicked} fill="#8884d8" label={({name, value})=>name}>
              {
                pieData.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]} key={`cell-${index}`}/>)
              }
            </Pie>
            <Tooltip/>
          </PieChart>
        </section>
      </div>
    );
  }
}

AggregatedOrders.propTypes = {
  aggregateBy: React.PropTypes.object.isRequired,
  orders: React.PropTypes.array.isRequired,
  onAggregatedClicked: React.PropTypes.func.isRequired
}

export default AggregatedOrders;
