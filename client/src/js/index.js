import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import 'babel-polyfill';

ReactDOM.render(<App />, document.getElementById('content'));
