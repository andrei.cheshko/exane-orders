import axios from 'axios';

import {
  ORDERS_FETCH_BEGIN,
  ORDERS_FETCH_SUCCESS,
  ORDERS_FETCH_ERROR
} from './actionTypes.js';


function requestOrders() {
  return {
    type: ORDERS_FETCH_BEGIN
  }
}

function ordersReceived(orders) {
  return {
    type: ORDERS_FETCH_SUCCESS,
    orders: orders,
    receivedAt: Date.now()
  }
}

function requestError(error) {
  console.error(error);
  return {
    type: ORDERS_FETCH_ERROR,
    error: error
  }
}

//
// If onlyNew is set to true, we will fetch new orders from
// /ws/neworders, otherwise we will fetch orders from /ws/orders
export function fetchOrders(onlyNew) {

  // Thunk middleware will pass dispatch into fetchOrders
  return function (dispatch) {
    dispatch(requestOrders());

    const url = `http://localhost:8080/ws/${onlyNew ? 'neworders' : 'orders'}`;

    return axios.get(url)
      .then(response => {
        dispatch(ordersReceived(response.data));
      })
      .catch(error => dispatch(requestError(error)));
  }
}
