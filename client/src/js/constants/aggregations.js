const AGGREGATE = {
  NONE: {label: 'None', value: 'none'},
  SYMBOL: {label: 'Symbol', value: 'symbol'},
  SIDE: {label: 'Side', value: 'side'},
  SECTOR: {label: 'Sector', value: 'sector'},
  CLIENT: {label: 'Client', value: 'client'},
  CLIENT_TYPE: {label: 'Client Type', value: 'clientType'},
  TRADER: {label: 'Trader', value: 'trader'}
}

export default AGGREGATE;
