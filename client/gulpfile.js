var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
var sass = require('gulp-sass');

function compileJS(watch) {
  var bundler = browserify('./src/js/index.js', { debug: true }).transform(babel, {presets: ['react', 'es2015']});

  function rebundle() {
    return bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./build/js'));
  }

  if (watch) {
    bundler = watchify(bundler);
    bundler.on('log', function(msg) {
      console.log(msg);
    }).on('update', function() {
      console.log('-> bundling...');
      rebundle();
    });
  }

  return rebundle();
}

// watch sass and js
function watch() {
  return compileJS(true);
};

function sass() {
 return gulp.src('./src/sass/**/*.scss')
   .pipe(sass().on('error', sass.logError))
   .pipe(gulp.dest('./build/css'));
}


gulp.task('js', function() { return compileJS()});

gulp.task('sass', function() {
  return gulp.src('./src/sass/**/*.scss')
   .pipe(sass({outputStyle: 'compressed', includePaths: ['./node_modules']}).on('error', sass.logError))
   .pipe(gulp.dest('./build/css'));
});

gulp.task('html', function() {
  return gulp.src('./src/index.html').pipe(gulp.dest('./build'));
});

gulp.task('build', ['sass','html', 'js']);

gulp.task('watch', function() { return watch(); });

gulp.task('default', ['build']);
