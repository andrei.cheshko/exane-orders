Requirements:  
- maven v3+  
- node v6+ / npm v3+  

Setting up back-end service:  
1) Make sure you are in project's *server* subdirectory - `cd ${project}/server`  
2) Install dependencies - `mvn clean install`  
3) Start Jetty Server - `mvn jetty:run`  
4) The back-end server will start on http://localhost:8080  
  
  
Setting up front-end:  
1) Make sure you are in project's *client* subdirectory - `cd ${project}/client`  
2) Install dependencies - `npm i`  
3) Build client code - `npm run gulp`  
4) Start client server - `npm start`  
5) Client-side server will start on http://localhost:3000  
  
Visit http://localhost:3000 to load the application.
