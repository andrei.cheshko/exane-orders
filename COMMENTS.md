1) Due to the fact that I got this task unexpectedly and was travelling this weekend,
   I could only dedicate ~6 hours to it. Approx. schedule:
   - 1 hour to familiarise with backend and do slight changes to config
   - 1 hour on thinking of structure and researching libraries
   - 3 hours on dev
   - 1 hour on testing/bug fixing

2) I had to do minor adjustments to the backend server configuration:
   - added jetty as dependency to be able to start a dev server.
   - Due to the fact that backend server is running on port 8080 and frontend
     on 3000, I had to configure cross-domain access. I had to spend some time,
     because I never configured Jetty before, but was able to get cross-domain filter
     working by adding &lt;filter&gt; to web.xml inside WebContent/WEB-INF and copying
     jetty-servlets.jar to WebContent/WEB-INF/lib

3) The client page loads in AGGREGATED view (grouped by SECTOR), clicking in grid's
   row or charts slice, will take you to ORDERS view filtered by that group name
   you clicked on. In the ORDERS view, you can filter the orders by specifying
   filters for each column. Above the table, there will be a Select box, which
   allows to select a group by which to aggregate orders and take you to AGGREGATED view.
   Given the endpoints and the description of the task, I thought these 2 screens would be
   enough.

3) Client Side is written in *ES6* with following tech stack:
  - react
  - redux
  - express
  - gulp
  - babel
  - sass  
  
  with support of following libraries:  
  - recharts
  - axios
  - react-select

4) Client side code entry point is in *${project}/client/src/js/App.jsx*, you will find there
   a constant *POLLING_FREQ_IN_SECS*, which dictates how often client side polls server. If
   you going to change the value, you would need to rebuild the client side by running `gulp`
   inside `${project/client` directory.

5) I didn't want to use */ws/aggregateOrders* endpoint, because, I noticed the endpoint, doesn't
   just aggregate orders, it also creates new orders. I wanted to maintain a state of all orders
   on the client side (and this state to be in sync with server), so I did
   *aggregation and filtering on the client side*.

6) Originally, I wanted to use [react-data-grid](https://github.com/adazzle/react-data-grid),
   because of its support of filtering and sorting and whats most important its *rendering speed*
   (rendering of rows only visible in fixed height container). Unfortunetly, I had some problems
   while using the component and didn't have time to fully understand and resolve it.
   I was forced to quickly create my own Table component (with filtering), but its not very performant
   and rendering is slowed down when Table has a lot of rows. I didn't have time to optimize it.

7) Since I was short on time, I didn't have time clean up the code. You will, probably find magic strings and
   some code that could be refactored. In 5 hours, I was only able to make the app work and didn't have much
   time to improve the quality of the code. I also didn't spent any time to make the app look 'pretty' I just
   focused on componentns functionality.

8) I didn't do anything to optimize client side code for production.

Cheers! Enjoyed working on this and looking forward to hearing from you.
