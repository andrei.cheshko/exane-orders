This project can be build using maven (https://maven.apache.org/)
Use:
mvn clean install

You also can use Eclipse with maven plugin.

After build deploy RestfulWebServiceTest-0.0.1-SNAPSHOT.war to an application server
or use Eclipse "Run on Server" to run.

Urls supported:
http://<hist>:<port>/RestfulWebServiceTest/ws/orders
http://<hist>:<port>/RestfulWebServiceTest/ws/neworders
http://<hist>:<port>/RestfulWebServiceTest/ws/aggregateorders?groupBy=<field>
