package com.exane.test.controller;

public class Order {
	private String clOrdID;
	private String symbol;
	private String side;
	private Double quantity;
	private String sector;
	private String client;
	private String clientType;
	private String trader;
	
	public Order(String clOrdID, String symbol, String side, Double quantity, String sector, String client, String clientType,
			String trader) {
		this.clOrdID = clOrdID;
		this.symbol = symbol;
		this.side = side;
		this.quantity = quantity;
		this.sector = sector;
		this.client = client;
		this.clientType = clientType;
		this.trader = trader;
	}
	
	public String getSymbol() {
		return symbol;
	}
	public String getSide() {
		return side;
	}
	public Double getQuantity() {
		return quantity;
	}
	public String getSector() {
		return sector;
	}
	public String getClient() {
		return client;
	}
	public String getClientType() {
		return clientType;
	}
	public String getTrader() {
		return trader;
	}
	public String getClOrdID() {
		return clOrdID;
	}

}
