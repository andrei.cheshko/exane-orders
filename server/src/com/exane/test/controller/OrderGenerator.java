package com.exane.test.controller;

import java.beans.ConstructorProperties;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class OrderGenerator {
	private List<Listing> listings;
	private List<Client> clients;
	private List<String> traders;
	
	private Random random = new Random();
	
	@ConstructorProperties({"listings", "clients", "traders"})
	public OrderGenerator(List<Listing> listings, List<Client> clients, List<String> traders) {
		this.listings = listings;
		this.clients = clients;
		this.traders = traders;
	}

	public Order createRandomOrder() {
		
		Listing listing = listings.get(random.nextInt(listings.size()));
		Client client = clients.get(random.nextInt(clients.size() + 2) % clients.size());
		String trader = traders.get(random.nextInt(traders.size()));
		String side = random.nextInt(3) == 0 ? "Buy" : "Sell";
		double quantity = Math.ceil(Math.abs(random.nextGaussian() * 1000));
		String clOrdID = UUID.randomUUID().toString();
		
		return new Order(clOrdID, listing.getSymbol(), side, quantity, listing.getSector(), client.getClientId(), client.getClientType(),
				trader);
	}
	
}
