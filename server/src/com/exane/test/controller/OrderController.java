package com.exane.test.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
	
	private LinkedList<Order> orders = new LinkedList<Order>();
	
	@Autowired
	private OrderGenerator orderGenerator;
	
	private static final int NUM_ORDERS = 5;

	@RequestMapping(value = "/orders", method = RequestMethod.GET, headers="Accept=application/json") 
	public List<Order> getOrders() {
		
		return createOrders();
	}
	
	@RequestMapping(value = "/neworders", method = RequestMethod.GET, headers="Accept=application/json") 
	public List<Order> newOrders() {
		LinkedList<Order> orders = createOrders();
		return orders.subList(orders.size() - NUM_ORDERS, orders.size());
	}
	
	private synchronized LinkedList<Order> createOrders() {
		if (orders.size() > 10000) {
			for (int i = 0; i < NUM_ORDERS; i++) {
				orders.removeFirst();
			}
		}
		
		for (int i = 0; i < NUM_ORDERS; i++) {
			orders.add(orderGenerator.createRandomOrder());
		}
		
		return orders;
	}

	@RequestMapping(value = "/aggregateorders", method = RequestMethod.GET, headers="Accept=application/json") 
	public Map<String, Double> getAggregatedOrders(@RequestParam(value="groupBy", defaultValue="sector") String groupBy) {
		TreeMap<String, Double> map = new TreeMap<String, Double>();
		LinkedList<Order> orders = createOrders();
		for (Order order : orders) {
			String key = null;
			switch (groupBy) {
				case "symbol":
					key = order.getSymbol();
					break;
				case "side":
					key = order.getSide();
					break;
				case "sector":
					key = order.getSector();
					break;
				case "client":
					key = order.getClient();
					break;
				case "clientType":
					key = order.getClientType();
					break;
				case "trader":
					key = order.getTrader();
					break;
			}
			Double val = map.get(key);
			if (val != null)
				map.put(key, val + order.getQuantity());
			else
				map.put(key, order.getQuantity());
			
		}
		return map;
	}
	
}
